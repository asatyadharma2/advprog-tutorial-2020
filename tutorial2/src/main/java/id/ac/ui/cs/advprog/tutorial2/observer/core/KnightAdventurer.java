package id.ac.ui.cs.advprog.tutorial2.observer.core;

public class KnightAdventurer extends Adventurer {

    public KnightAdventurer(String name, Guild guild) {
        super(name, guild);
    }

    public void update(){
        if (this.name.equalsIgnoreCase("Knight")) {
            this.getQuests().add(this.guild.getQuest());
        }
        }
}
