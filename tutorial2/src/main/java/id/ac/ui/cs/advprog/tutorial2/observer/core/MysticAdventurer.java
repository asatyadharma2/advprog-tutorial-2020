package id.ac.ui.cs.advprog.tutorial2.observer.core;

public class MysticAdventurer extends Adventurer {
    public MysticAdventurer(String name, Guild guild) {
        super(name, guild);
    }
    public void update(){
        boolean condition1 = this.name.equalsIgnoreCase("Mystic");
        boolean condition2 = this.guild.getQuestType().equalsIgnoreCase("D")
                ||this.guild.getQuestType().equalsIgnoreCase("E");
        if (condition1 && condition2) {
            this.getQuests().add(this.guild.getQuest());
        }
    }
}
