package id.ac.ui.cs.advprog.tutorial2.observer.core;

public class AgileAdventurer extends Adventurer{

    public AgileAdventurer(String name, Guild guild) {
        super(name, guild);

    }
    public void update(){
        boolean condition3 = this.name.equalsIgnoreCase("Agile");
        boolean condition4 = this.guild.getQuestType().equalsIgnoreCase("D")
                || this.guild.getQuestType().equalsIgnoreCase("R");
        if (condition3 && condition4){
                this.getQuests().add(this.guild.getQuest());
        }
    }
}


