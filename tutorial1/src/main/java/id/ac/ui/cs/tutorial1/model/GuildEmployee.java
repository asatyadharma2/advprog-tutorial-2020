package id.ac.ui.cs.tutorial1.model;

import java.util.Date;

public abstract class GuildEmployee {
    private String name;
    private String familyName;
    private String id;
    private Date birthDate;

    public GuildEmployee(String name, String familyName, String id, Date birthDate) {
        this.name = name;
        this.familyName = familyName;
        this.id = id;
        this.birthDate = birthDate;
    }

    public abstract String getWork();

    public String getName() {
        return name;
    }

    public String getFamilyName() {
        return familyName;
    }

    public String getId() {
        return id;
    }

    public Date getBirthDate() {
        return birthDate;
    }
}


