package id.ac.ui.cs.tutorial3.repository;

import java.util.List;
import java.util.ArrayList;

import id.ac.ui.cs.tutorial3.core.Synthesis;
import org.springframework.stereotype.Repository;


public class SynthesisRepository {
	private List<Synthesis> synthList = new ArrayList<>();

	public Synthesis addSynthesis(Synthesis synth){
		synthList.add(synth);
		return synth;
	}

	public List<Synthesis> getSynthesises(){
		return synthList;
	}

	public void emptyRepo(){
		synthList.clear();
	}
}